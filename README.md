# KiCad\_library

This is a collection of KiCAD component libraries for KiCAD 4 and 5 which I use and you
may find useful for your designs.

Currently it includes schematic symbols and PCB footprints. It may include 3D shapes later.

This work is available under a Creative Commons Attribution 4.0 International License (CC BY 4.0)

For the purposes of this license the title of this work is KiCAD component libraries.

It is licensed by Steve Bollinger and the original work was published (and may still
be available) at https://bitbucket.org/slagdang/kicad_library .

Information on the CC BY 4.0 license is available at http://creativecommons.org/licenses/by/4.0/
in both human-readable form and fully specified legal terms.
